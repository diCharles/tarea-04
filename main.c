/*
 * Copyright 2016-2019 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    TAREA04_Drivers.c
 * @brief   Application entry point.
 */
#include "rtos_gpio.h"

gpio_input_config_t g_sw2;
gpio_input_config_t g_sw3;
void sw2_task(void * args)
{
	/** task gets blocked here until a portC pin is interrupted*/
	//rtos_wait_input_gpio_portC();
	printf("interruption on port C \n");

}

void sw3_task(void * args)
{
	/** task gets blocked here until a portA pin is interrupted*/
	//rtos_wait_input_gpio_portA();

	printf("interruption on port A \n");

}


int main(void) {

	/** init GPIOs, in this case gpios for K64 onboard switches*/
	g_sw2.gpio_port = GPIO_C ;
	g_sw2.gpio_pin= 6 ;
	g_sw2.interruptEgde = INTR_FALLING_EDGE ;
	g_sw2.priority_threshold= PRIORITY_10 ;
	g_sw2.priority = PRIORITY_2 ;

	g_sw3.gpio_port = GPIO_A ;
	g_sw3.gpio_pin= 4 ;
	g_sw3.interruptEgde = INTR_FALLING_EDGE ;
	g_sw3.priority_threshold= PRIORITY_10 ;
	g_sw3.priority = PRIORITY_3 ;

	rtos_input_gpio_init(g_sw2);
	rtos_input_gpio_init(g_sw3);
	/** dummy ID for task*/
	static uint8_t  task_id = 0 ;
	//xTaskCreate(sw2_task, "sw2_task", configMINIMAL_STACK_SIZE, (void * )&task_id, 1, NULL);
	//xTaskCreate(sw3_task, "sw3_task", configMINIMAL_STACK_SIZE, (void *) &task_id, 1 , NULL);

	//vTaskStartScheduler();

	/** application must never reach this area*/
	while(1) {

	}
	return 0 ;
}

