/*
 * rtos_gpio.c
 *
 *  Created on: 15/10/2019
 *      Author: charl
 */

#include "rtos_gpio.h"

SemaphoreHandle_t      g_portA_sem;
SemaphoreHandle_t      g_portB_sem;
SemaphoreHandle_t      g_portC_sem;
SemaphoreHandle_t      g_portD_sem;
SemaphoreHandle_t      g_portE_sem;


static void gpioA_callback(void)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR( g_portA_sem, NULL );
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
static void gpioB_callback(void)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR( g_portB_sem, NULL );
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
static void gpioC_callback(void)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR( g_portB_sem, NULL );
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
static void gpioD_callback(void)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR( g_portD_sem, NULL );
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
static void gpioE_callback(void)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR( g_portE_sem, NULL );
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}



init_status_t rtos_input_gpio_init(gpio_input_config_t config)
{
	init_status_t retval = SUCCESS ;
	/** init input gpio pin */
	/** enabling pullup for input GPIO*/
	uint32_t input_intr_config = GPIO_MUX1|GPIO_PE|GPIO_PS;
	if(0 != (config.priority_threshold && config.priority))
	{
		/**Sets the threshold for interrupts, if the interrupt has higher priority constant that the BASEPRI, the interrupt will not be attended*/
		NVIC_set_basepri_threshold(config.priority_threshold);
		/**Enables and sets a particular interrupt and its priority*/
		NVIC_enable_interrupt_and_priotity(PORTC_IRQ,config.priority);
		input_intr_config|=config.interruptEgde;
		NVIC_global_enable_interrupts;
	}
	GPIO_clock_gating(config.gpio_port);
	GPIO_pin_control_register(config.gpio_port, config.gpio_pin, &input_intr_config);
	GPIO_data_direction_pin(config.gpio_port, GPIO_PIN_INPUT, config.gpio_pin);
	/** set callback*/
	switch (config.gpio_port)
	{
	case GPIO_A:
		PORTA_IRQ_Callback(gpioA_callback);
		vSemaphoreCreateBinary(g_portA_sem);
		break;
	case GPIO_B:
		PORTB_IRQ_Callback(gpioB_callback);
		vSemaphoreCreateBinary(g_portB_sem);
		break;
	case GPIO_C:
		PORTC_IRQ_Callback(gpioC_callback);
		vSemaphoreCreateBinary(g_portC_sem);
		break;
	case GPIO_D:
		PORTA_IRQ_Callback(gpioD_callback);
		vSemaphoreCreateBinary(g_portD_sem);
		break;
	case GPIO_E:
		PORTE_IRQ_Callback(gpioE_callback);
		vSemaphoreCreateBinary(g_portE_sem);
		break;
	case GPIO_F:
		retval = FAIL ;
		break;
	}
	return (retval) ;
}

void rtos_wait_input_gpio_portA(void)
{
	if( xSemaphoreTake( g_portA_sem, portMAX_DELAY) == pdTRUE )
	{

	}
}
void rtos_wait_input_gpio_portB(void)
{
	if( xSemaphoreTake( g_portB_sem, portMAX_DELAY) == pdTRUE )
	{

	}
}

void rtos_wait_input_gpio_portC(void)
{
	if( xSemaphoreTake( g_portC_sem, portMAX_DELAY) == pdTRUE )
	{

	}
}

void rtos_wait_input_gpio_portD(void)
{

	if( xSemaphoreTake( g_portD_sem, portMAX_DELAY) == pdTRUE )
	{

	}
}


