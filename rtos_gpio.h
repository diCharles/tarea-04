/*
 * rtos_gpio.h
 *
 *  Created on: 15/10/2019
 *      Author: charl
 */

#ifndef RTOS_GPIO_H_
#define RTOS_GPIO_H_


#include "NVIC.h"
#include "GPIO.h"
#include "Bits.h"

#include <stdio.h>
#include <stdlib.h>

#include "clock_config.h"
#include "FreeRTOS.h"
#include "projdefs.h"
#include "task.h"
#include "portable.h"
#include "FreeRTOSConfig.h"
#include "semphr.h"




typedef struct
{
	gpio_port_name_t gpio_port;
	bit_t gpio_pin;

	priority_level_t priority_threshold ;
	priority_level_t priority;
	uint32_t interruptEgde;
}gpio_input_config_t ;

typedef enum{
	SUCCESS,
	FAIL
}init_status_t;

init_status_t rtos_input_gpio_init(gpio_input_config_t config);

void rtos_wait_input_gpio_portA(void);
void rtos_wait_input_gpio_portB(void);
void rtos_wait_input_gpio_portC(void);
void rtos_wait_input_gpio_portD(void);
void rtos_wait_input_gpio_portE(void);

#endif /* RTOS_GPIO_H_ */
