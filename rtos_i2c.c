/*
 * rtos_i2c.c
 *
 *  Created on: Oct 14, 2019
 *      Author: wright
 */

#include "rtos_i2c.h"

#include "fsl_i2c.h"
#include "fsl_clock.h"
#include "fsl_port.h"

#include "FreeRTOS.h"
#include "semphr.h"
#define NUMBER_OF_SERIAL_PORTS (3)
#define I2C_MASTER_CLK_FREQ CLOCK_GetFreq(I2C0_CLK_SRC)

typedef struct
{
	uint8_t is_init;
	i2c_master_handle_t master_handle;
	i2c_slave_handle_t slave_handle;

	SemaphoreHandle_t mutex_tx;
	SemaphoreHandle_t tx_sem;
}rtos_i2c_handle_t;

static rtos_i2c_handle_t i2c_handles[NUMBER_OF_SERIAL_PORTS] = {0};


static inline void enable_port_clock(rtos_i2c_port_t);
static inline I2C_Type * get_i2c_base(rtos_i2c_number_t);
static inline PORT_Type * get_port_base(rtos_i2c_port_t);
static void i2c_callback_master(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *userData)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	if (kStatus_I2C_Idle == status)
	{
		if(I2C0 == base)
		{
			xSemaphoreGiveFromISR(i2c_handles[rtos_i2c0].tx_sem, &xHigherPriorityTaskWoken );
		}
		else
		{
			xSemaphoreGiveFromISR(i2c_handles[rtos_i2c1].tx_sem, &xHigherPriorityTaskWoken );
		}
	}
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
};

rtos_i2c_flag_t rtos_i2c_send(rtos_i2c_number_t i2c_number,uint8_t * buffer, uint16_t lenght)
{

}
//rtos_i2c_flag_t rtos_i2c_receive(rtos_i2c_number_t i2c_number, uint8_t * buffer, uint16_t lenght);

rtos_i2c_flag_t rtos_i2c_init(rtos_i2c_config_t config)
{
	rtos_i2c_flag_t retval;

	/** habilitar master*/
	i2c_master_config_t masterConfig;


	I2C_MasterGetDefaultConfig(&masterConfig);

	masterConfig.baudRate_Bps =  config.master.baudrate ;
	masterConfig.enableMaster = true;
	I2C_Type * i2c_port = get_i2c_base(config.master.i2c_number);

	uint32_t sourceClock = I2C_MASTER_CLK_FREQ ;
	I2C_MasterInit(i2c_port, &masterConfig, sourceClock);

	I2C_MasterTransferCreateHandle(i2c_port,& (i2c_handles[config.master.i2c_number].master_handle), i2c_callback_master, NULL);


	/**  interrupcion del master*/


	/** crear semaforo  y mutex*/
	if(config.master.i2c_number < NUMBER_OF_SERIAL_PORTS)
	{
		if(!i2c_handles[config.master.i2c_number].is_init)
		{
			i2c_handles[config.master.i2c_number].mutex_tx = xSemaphoreCreateMutex();

			i2c_handles[config.master.i2c_number].tx_sem = xSemaphoreCreateBinary();

			enable_port_clock(config.master.port);
			PORT_SetPinMux(get_port_base(config.master.port),config.master.SDA_pin,config.master.pin_mux);
			PORT_SetPinMux(get_port_base(config.master.port),config.master.SCL_pin,config.master.pin_mux);

		}
		if(rtos_i2c0 ==  config.master.i2c_number)
		{
			NVIC_SetPriority(I2C0_IRQn, 1);

		}
		if(rtos_i2c1 ==  config.master.i2c_number)
		{
			NVIC_SetPriority(I2C1_IRQn, 1);

		}
		if(rtos_i2c0 ==  config.master.i2c_number)
		{
			return rtos_i2c_fail ;

		}
		i2c_handles[config.master.i2c_number].is_init = 1;
		retval = rtos_i2c_sucess;

	}

	return retval;
}

static inline void enable_port_clock(rtos_i2c_port_t port)
{
	switch(port)
	{
	case rtos_i2c_portA:
		CLOCK_EnableClock(kCLOCK_PortA);
		break;
	case rtos_i2c_portB:
		CLOCK_EnableClock(kCLOCK_PortB);
		break;
	case rtos_i2c_portC:
		CLOCK_EnableClock(kCLOCK_PortC);
		break;
	case rtos_i2c_portD:
		CLOCK_EnableClock(kCLOCK_PortD);
		break;
	case rtos_i2c_portE:
		CLOCK_EnableClock(kCLOCK_PortE);
		break;
	}
}

static inline I2C_Type * get_i2c_base(rtos_i2c_number_t i2c_number)
{
	I2C_Type * retval = I2C0;
	switch(i2c_number)
	{
	case rtos_i2c0:
		retval = I2C0;
		break;
	case rtos_i2c1:
		retval = I2C1;
		break;
	case rtos_i2c2:
		retval = I2C2;
	}
	return retval;
}

static inline PORT_Type * get_port_base(rtos_i2c_port_t port)
{
	PORT_Type * port_base = PORTA;
	switch(port)
	{
	case rtos_i2c_portA:
		port_base = PORTA;
		break;
	case rtos_i2c_portB:
		port_base = PORTB;
		break;
	case rtos_i2c_portC:
		port_base = PORTC;
		break;
	case rtos_i2c_portD:
		port_base = PORTD;
		break;
	case rtos_i2c_portE:
		port_base = PORTE;
		break;
	}
	return port_base;
}
