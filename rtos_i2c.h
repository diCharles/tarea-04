/*
 * rtos_i2c.h
 *
 *  Created on: Oct 14, 2019
 *      Author: wright
 */

#ifndef RTOS_I2C_H_
#define RTOS_I2C_H_

#include <stdint.h>

typedef enum {rtos_i2c0,rtos_i2c1,rtos_i2c2} rtos_i2c_number_t;
typedef enum {rtos_i2c_portA,rtos_i2c_portB,rtos_i2c_portC,rtos_i2c_portD,rtos_i2c_portE} rtos_i2c_port_t;
typedef enum {rtosi2c_master,rtos_i2c_slave} rtos_i2c_mstr_slv_t;
typedef enum {rtos_i2c_write, rtos_i2c_read} rtos_i2c_wr_t;
typedef enum {rtos_i2c_sucess,rtos_i2c_fail} rtos_i2c_flag_t;

typedef struct
{
	uint32_t  baudrate;
	rtos_i2c_number_t i2c_number;
	rtos_i2c_port_t port;
	uint8_t SDA_pin;
	uint8_t SCL_pin;
	uint8_t pin_mux;
}rtos_i2c_master_config_t;

typedef struct
{
	uint32_t  baudrate;
	rtos_i2c_number_t i2c_number;
	rtos_i2c_port_t port;
	uint8_t slave_addr;
	uint8_t data;
	uint8_t SDA_pin;
	uint8_t SCL_pin;
	uint8_t pin_mux;
}rtos_i2c_slave_config_t;

typedef struct
{
	rtos_i2c_master_config_t master;
	rtos_i2c_slave_config_t slave;

}rtos_i2c_config_t;


rtos_i2c_flag_t rtos_i2c_init(rtos_i2c_config_t config);
rtos_i2c_flag_t rtos_i2c_send(rtos_i2c_number_t i2c_number,uint8_t * buffer, uint16_t lenght);
//rtos_i2c_flag_t rtos_i2c_receive(rtos_i2c_number_t i2c_number, uint8_t * buffer, uint16_t lenght);


#endif /* RTOS_I2C_H_ */
